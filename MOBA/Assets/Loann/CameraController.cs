﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float panSpeed = 20f;
    public float panBorderThickness = 10f;
    public Vector2 panLimit;

    public float scrollSpeed = 20f;
    public float minY = 20f;
    public float maxY = 120f;
    public bool a = false;

    public void Start()
    {
        a = false;
    }

    void Update() // Permet de controler la caméra pour se déplacer à travers la map
    {
        Vector3 pos = transform.position;

        if (Input.mousePosition.y >= Screen.height - panBorderThickness) // Permet de faire bouger la caméra vers le haut 
        {
            pos.z += panSpeed * Time.deltaTime;
        }
        if (Input.mousePosition.y <= panBorderThickness) // Permet de faire bouger la caméra vers le bas
        {
            pos.z -= panSpeed * Time.deltaTime;
        }
        if (Input.mousePosition.x >= Screen.height - panBorderThickness) // Permet de faire bouger la caméra vers la gauche
        {
            pos.x += panSpeed * Time.deltaTime;
        }
        if (Input.mousePosition.x <= panBorderThickness) // Permet de faire bouger la caméra vers la droite 
        {
            pos.x -= panSpeed * Time.deltaTime;
        }

        float scroll = Input.GetAxis("Mouse ScrollWheel"); // permet de zoomer et dézoomer
        pos.y -= scroll * scrollSpeed * 100f * Time.deltaTime;
        
        pos.x = Mathf.Clamp(pos.x, -panLimit.x, panLimit.x); // attribue une limite en x
        pos.y = Mathf.Clamp(pos.y, minY, maxY);
        pos.z = Mathf.Clamp(pos.z, -panLimit.y, panLimit.y); // attribue une limite en z
        
        transform.position = pos;
        if (Input.GetButtonDown("Space")) // Appuie sur le bouton space pour changer sa caméra
        {
            panBorderThickness = 1000f;
            panLimit = new Vector2(0, 0);
        }
        else
        {
            panBorderThickness = 10f;
            panLimit = new Vector2(15, 30);
        } 
        if (Input.GetButton("a")) // appuyer sur a pour lock la caméra
        {
            panBorderThickness = 1000f;
            panLimit = new Vector2(0, 0);
            a = true;
        }
        if (a == true)
        {
            panBorderThickness = 1000f;
            panLimit = new Vector2(0, 0);
        }
        if (Input.GetButton("z")) // appuyer sur z pour unlock la caméra
        {
            a = false;
            panBorderThickness = 10f;
            panLimit = new Vector2(15, 30);
        }
    }
}
