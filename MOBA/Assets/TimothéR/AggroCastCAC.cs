﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Rendering;

public class AggroCastCAC : MonoBehaviour
{
    public float cooldown = 2;
    public GameObject bullet;
    public Deplacement mob;
    public Vector3 origin;
    public Vector3 direction;
    public LayerMask layersEnnemi;
    public float maxdistance; 
    // Start is called before the first frame update
    void Start()
    {
        mob = GetComponent<Deplacement>();
    }

    // Update is called once per frame
   public void Update()
    {
        direction = transform.forward;
        origin = transform.position;
        RaycastHit[] ray = Physics.SphereCastAll(origin, 2, direction, maxdistance, layersEnnemi);
        foreach (RaycastHit mobit in ray)
        {// Pour chaque RaycastHit dans l'array debug.log + desactive le NavmeshAgent du mob et instantiate un Gameobject bullet 
            Debug.Log(mobit);
            
            if (mob.sbire.enabled == false && cooldown <= 0)
            {
                GameObject bullet;
                bullet = Instantiate(this.bullet, origin, Quaternion.identity);
                bullet.GetComponent<Rigidbody>().AddForce(new Vector3(100, 0, 0));
                cooldown = 2;
            }

            if (mobit.transform.CompareTag("SbireEnnemi") && gameObject.CompareTag("SbireAllie"))
            {
                mob.sbire.enabled = false;
            }
            if (mobit.transform.CompareTag("SbireAllie") && gameObject.CompareTag("SbireEnnemi"))
            {
                mob.sbire.enabled = false;
            }
            if (mobit.transform.gameObject.CompareTag("Player")&& gameObject.CompareTag("SbireEnnemi"))
            {
                mobit.transform.gameObject.GetComponent<MovePlayerSam>().LooseLifePlayer(2);
            }

            cooldown -= Time.deltaTime;
        }


    }

    private void OnDrawGizmos()
    {// debug une sphere dans la scene
        Gizmos.color = Color.magenta;
        Gizmos.DrawWireSphere(origin + direction,maxdistance);
    }
}
