﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvokeMinionCannon : MonoBehaviour
{    
    public GameObject minionCannon;
    public GameObject spawnminionCannon;
    public Transform spawnMinionCannon;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SpawnDuMinionCannon", 5f, 5f);
    }
    public void SpawnDuMinionCannon()
    {// Permet de faire spawn l'object minionCannon a la pos de spawnMinionCANNON et lance un debuglog
        Debug.Log("Spawn du minion Cannon ! ");
        Instantiate(minionCannon, spawnMinionCannon.position, Quaternion.identity);
    }

  
    // Update is called once per frame
    void Update()
    {
        
    }
}
