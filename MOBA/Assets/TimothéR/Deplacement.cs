﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Deplacement : MonoBehaviour
{
    public NavMeshAgent sbire;
    public Transform finishdirection;

    public int sbireLife;
    // Start is called before the first frame update
   public void Start()
    {
        sbire = GetComponent((typeof(NavMeshAgent))) as UnityEngine.AI.NavMeshAgent;
        DestinationSbire();
    }

    public void DestinationSbire()
    {// Set la destination du navmesh à finishdirection.position
        sbire.SetDestination(finishdirection.position);
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Bullet"))
        {
            sbireLife -= 2;
            Destroy(other.gameObject);
        }
    }

    private void Update()
    {
        if (sbireLife <= 0)
        {
            Destroy(gameObject);
        }
    }

    public void LooseLifeSbire(int value)
    {
        sbireLife -= value;
    }
}
