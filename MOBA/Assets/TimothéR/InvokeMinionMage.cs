﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvokeMinionMage : MonoBehaviour
{     
    public GameObject minionMage;
    public GameObject spawnminionMage;
    public Transform spawnMinionMage;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SpawnDuMinionMage", 5f, 5f);
    }
    public void SpawnDuMinionMage()
    {// Permet de faire spawn l'object minionMage a la pos de spawnMinionMage et lance un debuglog
        Debug.Log("Spawn du minion Mage ! ");
        Instantiate(minionMage, spawnMinionMage.position, Quaternion.identity);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
