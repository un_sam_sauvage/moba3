﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AggroCastMage : MonoBehaviour
{
    public Deplacement mob;
    public Vector3 origin;
    public Vector3 direction;
    public LayerMask layersEnnemi;

    public float maxdistance;

    // Start is called before the first frame update
    void Start()
    {
        mob = GetComponent<Deplacement>();
    }

    // Update is called once per frame
    void Update()
    {
        direction = transform.forward;
        origin = transform.position;
        RaycastHit[] ray = Physics.SphereCastAll(origin, 6, direction, maxdistance, layersEnnemi);
        foreach (RaycastHit mobit in ray)
        {
            {
                // Pour chaque RaycastHit dans l'array debug.log + desactive le NavmeshAgent du mob
                Debug.Log(mobit);
                            
                if (mobit.transform.CompareTag("SbireEnnemi") && gameObject.CompareTag("SbireAllie"))
                {
                    mob.sbire.enabled = false;
                }
                if (mobit.transform.CompareTag("SbireAllie") && gameObject.CompareTag("SbireEnnemi"))
                {
                    mob.sbire.enabled = false;
                }

            }
        }

      
    }
    private void OnDrawGizmos()
    {
        // debug une sphere dans la scene
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(origin + direction, maxdistance);
    }
}