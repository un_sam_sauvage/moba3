﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.AI;

public class MovePlayerSam : MonoBehaviour
{
    public Player player;
    
    private NavMeshAgent _agent;
    
    private Transform _moveTarget;

    public GameObject panelDeath;

    public TextMeshProUGUI countdownDeath;

    private float _countdownDeathFloat = 10;
    private float _coutndownDeathFloatStart;
    private int _countdownDeathInt;
    private int _lifeStart;

    private Vector3 _positionStartPlayer;
    
    // Start is called before the first frame update
    void Start()
    {
        _coutndownDeathFloatStart = _countdownDeathFloat;
        _lifeStart = player.life;
        _positionStartPlayer = transform.position;
        _agent = GetComponent(typeof(NavMeshAgent)) as NavMeshAgent;
    }

    // Update is called once per frame
    void Update()
    {
        // si le joueur clique sur l'écran avec le bouton gauche de sa souris, le personnage va y aller en suivant un chemin établit par navmesh.
        if (Input.GetMouseButton(0) && _agent.enabled)
        {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 1000))
            {
                var clickPosition = hit.point;
                _agent.SetDestination(clickPosition);
            }
        }
//si le player n'a plus de vie, appelle la fonction death.
        if (player.life <= 0)
        {
            Death();
        }
        GetComponent<HUD>().updateHUD(player.life,player.mana,player.name);
    }
//le player perd de la vie
    public void LooseLifePlayer(int value)
    {
        player.life -= value;
        Debug.Log(player.life);
    }
// quand la vie du player tombe a 0, déplace le player a son point de spawn, l'empêche de bouger tant que le countdown de respawn n'est pas fini.
    public void Death()
    {

        panelDeath.SetActive(true);
        
        countdownDeath.text = _countdownDeathInt.ToString();
        _countdownDeathFloat -= 1 *Time.deltaTime;
        _countdownDeathInt = (int)Math.Ceiling(_countdownDeathFloat);
        transform.position = _positionStartPlayer;
        _agent.enabled = false;
        if (_countdownDeathInt <= 0)
        {
            panelDeath.SetActive(false);
            player.life = _lifeStart;
            _agent.enabled = true;
            _coutndownDeathFloatStart += 5;
            _countdownDeathFloat = _coutndownDeathFloatStart;
        }
    }
//quand le joueur entre en contact avec un buisson rend le joueur transparent.
    private void OnTriggerEnter(Collider other)
    {
        
        if (other.CompareTag("Bush"))
        {
            gameObject.GetComponent<Renderer>().material.color = new Color(0,0,0,0);
        }
        if (other.CompareTag("Spawn"))
        {
            player.life = _lifeStart;
        }
    }
//quand le joueur quitte un buisson le joueur redevient visible
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Bush"))
        {
            gameObject.GetComponent<Renderer>().material.color = new Color(0,0,0,1);
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        
        if (other.gameObject.CompareTag("BadTower"))
        {
            Debug.Log("touché");
            other.gameObject.GetComponent<Tower>().LooseLifeTower(1);
        }
        if (other.gameObject.CompareTag("SbireEnnemi"))           
        {
            other.gameObject.GetComponent<Deplacement>().LooseLifeSbire(5);
        }


    }
}
[Serializable]
public class Player
{
    public int life;
    public int mana;
    public string name;
}