﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.XR;

public class Tower : MonoBehaviour
{
    
    private float _sphereRadius = 7;
    public float maxDistance;
    private float _currentHitDistance;

    public LayerMask layerCollision;

    private Vector3 _origin;
    private Vector3 _direction;

    private int _lifeTower = 6;    

    private bool _playerTouch;
    private bool _playerAround;
    
    private GameObject _player;
    private void Start()
    {
        InvokeRepeating("PlayerDamage",0f,1f);
    }

    void Update()
    {
        //la tour regarde d'elle si il y a un player ennemi. si oui elle lui inflige des dégâts.
        _origin = transform.position;
        _direction = Vector3.forward;
        RaycastHit[] hits = Physics.SphereCastAll(_origin, _sphereRadius, _direction, maxDistance, layerCollision);
        foreach (var hit in hits)
        {
            if (hit.transform.CompareTag("SbireEnnemi")&& _playerAround ==false && gameObject.CompareTag("NiceTower"))
            {
                hit.transform.gameObject.GetComponent<Deplacement>().LooseLifeSbire(0);
            }
            else if (hit.transform.CompareTag("PlayerNice") && gameObject.CompareTag("BadTower"))
            {
                _player = hit.transform.gameObject;
                Debug.DrawRay(_origin,hit.transform.position-_origin,Color.red);
               _playerTouch = true;
            }
            else if (hit.transform.CompareTag("SbireAllie")&& _playerAround ==false && gameObject.CompareTag("BadTower"))
            {
                hit.transform.gameObject.GetComponent<Deplacement>().LooseLifeSbire(0);
            }

            _playerAround = false;
        }

        if (_lifeTower <= 0)
        {
            Destroy(gameObject);
        }
    }
    //envoie l'information au script du player qu'il a perdu de la vie.
    public void PlayerDamage()
    {
        if (_playerTouch)
        {
            _player.GetComponent<MovePlayerSam>().LooseLifePlayer(5);
        }
        _playerTouch = false;
    }

    public void LooseLifeTower(int value)
    {
        _lifeTower -= value;
        _playerAround = true;
    }
    
    private void OnDrawGizmosSelected()
    {
        Gizmos.color=Color.blue;
        Gizmos.DrawWireSphere(_origin+_direction*_currentHitDistance,_sphereRadius);
    }
}
