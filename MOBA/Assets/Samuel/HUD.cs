﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HUD : MonoBehaviour
{
    public TextMeshProUGUI life;
    public TextMeshProUGUI mana;
    public TextMeshProUGUI name;

    public void updateHUD(int value1,int value2,string value3)
    {
        life.text = "vie : " + value1.ToString();
        mana.text = "mana : " + value2.ToString();
        name.text = "nom :" + value3;
    }
}
