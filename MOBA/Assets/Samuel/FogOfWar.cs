﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FogOfWar : MonoBehaviour
{
    public GameObject fogOfWarPlane;

    public Transform playerTransform;

    public LayerMask foglayer;

    public float radius;
    private float _radiusSqr
    {
        get { return radius * radius; }
    }

    private Mesh _mesh;

    private Vector3[] _vertices;

    private Color[] _color;
    // Start is called before the first frame update
    void Start()
    {
        Initialize();
    }

    // Update is called once per frame
    void Update()
    {
        Ray r = new Ray(transform.position,playerTransform.position - transform.position);
        RaycastHit hit;
        if (Physics.Raycast(r, out hit, 100, foglayer, QueryTriggerInteraction.Collide))
        {
            for (int i = 0; i < _vertices.Length; i++)
            {
                Vector3 v = fogOfWarPlane.transform.TransformPoint(_vertices[i]);
                float dist = Vector3.SqrMagnitude(v - hit.point);
                if (dist < _radiusSqr)
                {
                    float alpha = Mathf.Min(_color[i].a, dist / _radiusSqr);
                    _color[i].a = alpha;
                }
            }
            UpdateColor();
            Initialize();
        }
    }

    void Initialize()
    {
        _mesh = fogOfWarPlane.GetComponent<MeshFilter>().mesh;
        _vertices = _mesh.vertices;
        _color = new Color[_vertices.Length];
        for (int i = 0; i < _color.Length; i++)
        {
            _color[i] = Color.black;
        }
        UpdateColor();
    }

    void UpdateColor()
    {
        _mesh.colors = _color;
    }
}
